export type NotificationType = {
    successfull: string,
    warning: string,
    error: string
}