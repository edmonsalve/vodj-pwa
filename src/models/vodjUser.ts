export type VodjUser = {
    uid: string;
    fullName: string;
    likes: any[],
    ownVideos: any[];
    followers: any[];
    following: any[];
    gender?: string;
    birthday?: string;
};