import * as Constants from "../utils/constants";

export default class YoutubeClient {
  apiKey: string;
  baseUrl: string = Constants.youtubeApiUrl;

  videos: any = null;

  constructor(apiKey) {
    this.apiKey = apiKey;
  }

  async getVideosFromYoutubeAPI(channelId: string) {
    let url = `${this.baseUrl}/search?key=${this.apiKey}&part=id,snippet&channelId=${channelId}&maxResults=50`;
    let response = await fetch(url);
    let body = await response.json();
    return body;
  }

   async getVideos(channelId: string) {
    const videoStoreKey = `videos-${channelId}`
    if (localStorage.hasOwnProperty(videoStoreKey)) {
        return JSON.parse(localStorage.getItem(`${videoStoreKey}`));
    } else {
        let videos: any = await this.getVideosFromYoutubeAPI(channelId);
        videos = this.extractVideoData(videos.items);
        this.storeVideosOfChannel(videoStoreKey, videos)
        return videos;
    }
  }

  extractVideoData(items) {
    let videos: any[] = [];
    items.map((item) => {
      if (item.id.kind == "youtube#video") {
        let video = {id: item.id.videoId, ...item.snippet};
        videos.push(video);
      }
    });
    return videos;
  }

  storeVideosOfChannel(key: string, videos: any[]) {
    if (!videos) return;

    localStorage.setItem(key, JSON.stringify(videos));
  }
}
