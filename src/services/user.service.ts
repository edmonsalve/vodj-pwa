export default class UserService {

    constructor() {}

    saveLoggedUser(user: any) {
        if (!user) return;
        localStorage.setItem("loggedUser", JSON.stringify(user));
    }

    getLoggedUser() {
        return JSON.parse(localStorage.getItem("loggedUser"));
    }
}