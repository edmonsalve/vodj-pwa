import { Component, h, State } from '@stencil/core';
import YoutubeClient from '../../services/youtube.service';
import * as Constants from "../../utils/constants"
import { VodjUser } from '../../models/vodjUser';
import UserService from '../../services/user.service';

const nav = document.querySelector('ion-nav');
declare var firebase: any;

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css',
  shadow: false
})
export class AppHome {
  
  constructor() {}

  userService: UserService = new UserService();

  @State() loggedUser: VodjUser;

  @State() videos: any[];

  youtubeClient: YoutubeClient = new YoutubeClient(Constants.youtubeApiKey)

  async componentDidLoad() {
    this.videos = await this.youtubeClient.getVideos(Constants.vodjYoutubeChannelId);
    this.loggedUser = this.userService.getLoggedUser();
  }

  goToEventDetail(ev: MouseEvent, video: any) {
    ev.preventDefault();
    nav.rootParams = { video }
    nav.setRoot("app-video-detail");
  }

  goToProfile() {
    nav.setRoot(`app-user-profile`);
  }


  goToLivePlayer() {
    nav.setRoot("app-video-player")
  }

  render() {

    if (!this.videos || !this.loggedUser) {
      return;
    }

    return (
      <div class="home-container">
        <header>
          <div class="container">				
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <nav class="navbar navbar-expand-lg navbar-light bg-dark1 justify-content-sm-start">
                  <a class="order-1 order-lg-0 ml-lg-0 ml-3 mr-auto" href="index.html"><img src="../assets/images/logo.png" alt="" /></a>
                  <button class="navbar-toggler align-self-start" type="button">
                    <i class="fas fa-bars"></i>
                  </button>
                  <div class="collapse navbar-collapse d-flex flex-column flex-lg-row flex-xl-row justify-content-lg-end bg-dark1 p-3 p-lg-0 mt1-5 mt-lg-0 mobileMenu" id="navbarSupportedContent">
                    <ul class="navbar-nav align-self-stretch">
                      <li class="nav-item active">
                        <a class="nav-link" href="index.html">
                          Home <span class="sr-only">(current)</span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link">
                          Timeline
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="discussions.html">
                          Personas
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="discussions.html">
                          Acerca de VODJ
                        </a>
                      </li>
                    </ul>
                    <a onClick={() => this.goToLivePlayer()} class="add-event">Estamos en vivo</a>
                  </div>
                  <ul class="group-icons">
                    <li><a href="search_result.html" class="icon-set"><i class="fas fa-search"></i></a></li>
                    <li class="dropdown">
                      <a href="#" class="icon-set dropdown-toggle-no-caret" role="button" data-toggle="dropdown">
                        <i class="fas fa-user-plus"></i>
                      </a>
                      <div class="dropdown-menu user-request-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#"><img src="/assets/images/user-dp-1.jpg" alt="" /></a>
                              <a href="#" class="user-title">Jassica William</a>
                            </div>
                            <button class="accept-btn">Accept</button>
                          </div>											
                        </div>	
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#"><img src="/assets/images/user-dp-1.jpg" alt=""  /></a>
                              <a href="#" class="user-title">Rock Smith</a>
                            </div>
                            <button class="accept-btn">Accept</button>
                          </div>											
                        </div>	
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#"><img src="/assets/images/user-dp-1.jpg" alt="" /></a>
                              <a href="#" class="user-title">Joy Doe</a>
                            </div>
                            <button class="accept-btn">Accept</button>
                          </div>											
                        </div>
                        <div class="user-request-list">
                          <a href="my_dashboard_all_requests.html" class="view-all">View All Friend Requests</a>
                        </div>	
                      </div>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="icon-set dropdown-toggle-no-caret" role="button" data-toggle="dropdown">
                        <i class="fas fa-envelope"></i>
                      </a>
                      <div class="dropdown-menu message-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#"><img src="/assets/images/user-dp-1.jpg" alt="" />
                                <div class="user-title1">Jassica William </div>
                                <span>Hey How are you John Doe...</span>
                              </a>
                            </div>
                            <div class="time5">2 min ago</div>
                          </div>											
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#"><img src="/assets/images/user-dp-1.jpg" alt="" />
                                <div class="user-title1">Rock Smith </div>
                                <span>Interesting Event! I will join this...</span>
                              </a>
                            </div>
                            <div class="time5">5 min ago</div>
                          </div>											
                        </div>	
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#"><img src="/assets/images/user-dp-1.jpg" alt="" />
                                <div class="user-title1">Joy Doe </div>
                                <span>Hey Sir! What about you...</span>
                              </a>
                            </div>
                            <div class="time5">10 min ago</div>
                          </div>											
                        </div>	
                        <div class="user-request-list">
                          <a href="my_dashboard_messages.html" class="view-all">View All Messages</a>
                        </div>	
                      </div>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="icon-set dropdown-toggle-no-caret" role="button" data-toggle="dropdown">
                        <i class="fas fa-bell"></i>
                      </a>
                      <div class="dropdown-menu notification-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#"><img src="/assets/images/user-dp-1.jpg" alt="" />
                                <div class="user-title1">Jassica William </div>
                                <span>comment on your video.</span>
                              </a>
                            </div>
                            <div class="time5">2 min ago</div>
                          </div>											
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#"><img src="/assets/images/user-dp-1.jpg" alt="" />
                                <div class="user-title1">Rock Smith</div>
                                <span>your order is accepted.</span>
                              </a>
                            </div>
                            <div class="time5">5 min ago</div>
                          </div>											
                        </div>	
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#"><img src="/assets/images/user-dp-1.jpg" alt="" />
                                <div class="user-title1">Joy Doe </div>
                                <span>your bill slip sent on your email.</span>
                              </a>
                            </div>
                            <div class="time5">10 min ago</div>
                          </div>											
                        </div>	
                        <div class="user-request-list">
                          <a href="my_dashboard_all_notifications.html" class="view-all">View All Notifications</a>
                        </div>	
                      </div>
                    </li>
                  </ul>
                  <div class="account order-1 dropdown">
                    <a href="#" class="account-link dropdown-toggle-no-caret" role="button" data-toggle="dropdown"> 
                      <div class="user-dp"><img src="/assets/images/dp.jpg" alt="" /></div>
                      <span>{this.loggedUser.fullName}</span>
                      <i class="fas fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu account-dropdown dropdown-menu-right">
                      <a class="link-item" onClick={() => this.goToProfile()}>Perfil</a>
                      <a class="link-item" href="my_dashboard_messages.html">Messages</a>											
                      <a class="link-item" href="my_dashboard_booked_events.html">Booked Events</a>
                      <a class="link-item" href="my_dashboard_credits.html">Credit <span class="right-cm">$100</span></a>
                      <a class="link-item" href="invite.html">Invite</a>
                      <a class="link-item" href="my_dashboard_setting_info.html">Setting</a>
                      <a class="link-item" href="login.html">Logout</a>									
                    </div>
                  </div>							
                </nav>
                <div class="overlay"></div>
              </div>					
            </div>					
          </div>
        </header>
      <main>	
        <div class="main-section">
          <div class="container">
            <div class="row">
              <div class="col-lg-3 col-md-5">
                <div class="main-left-sidebar">
                  <div class="user-data full-width">
                    <div class="user-profile">
                      <div class="username-dt dpbg-1">
                        <div class="usr-pic">
                          <img src="/assets/images/user-background.jpg" alt="" />
                        </div>
                      </div>
                      <div class="user-main-details">
                        <h4>{this.loggedUser.fullName}</h4>
                        <span><i class="fas fa-map-marker-alt"></i>Colombia</span>
                      </div>
                      <ul class="followers-dts">
                        <li>
                          <div class="followers-dt-sm">
                            <h4>Seguidores</h4>
                            <span>{this.loggedUser.followers.length}</span>
                          </div>
                        </li>
                        <li>
                          <div class="followers-dt-sm">
                            <h4>Seguidos</h4>
                            <span>{this.loggedUser.followers.length}</span>
                          </div>
                        </li>
                      </ul>
                      <div class="profile-link">
                        <a onClick={() => this.goToProfile()}>Ver Perfil</a>
                      </div>
                    </div>							
                  </div>	
                  <div class="user-data full-width">
                    <div class="categories-left-heading">
                      <h3>Categories</h3>							
                    </div>
                    <div class="categories-items">
                      <a class="category-item" href="#"><i class="fas fa-music"></i>Music</a>
                      <a class="category-item" href="#"><i class="fas fa-flag"></i>Festival</a>
                      <a class="category-item" href="#"><i class="fas fa-pen-nib"></i>Art</a>
                      <a class="category-item" href="#"><i class="fas fa-microphone-alt"></i>Club</a>
                      <a class="category-item" href="#"><i class="fas fa-grin-squint-tears"></i>Comedy</a>
                      <a class="category-item" href="#"><i class="far fa-futbol"></i>Sports</a>
                      <a class="category-item" href="#"><i class="fas fa-video"></i>Theatre</a>
                      <a class="category-item" href="#"><i class="fas fa-bullhorn"></i>Promotions</a>
                      <a class="category-item" href="#"><i class="fas fa-ellipsis-h"></i>Others</a>										
                    </div>
                  </div>
                  <div class="user-data full-width">
                    <div class="categories-left-heading">
                      <h3>Peoples</h3>							
                    </div>
                    <div class="sugguest-user">
                      <div class="sugguest-user-dt">
                        <a href="user_dashboard_activity.html"><img src="/assets/images/homepage/left-side/left-img-1.jpg" alt="" /></a>
                        <a href="user_dashboard_activity.html"><h4>Johnson</h4></a>
                      </div>
                      <button class="request-btn"><i class="fas fa-user-plus"></i></button>
                    </div>
                    <div class="sugguest-user">
                      <div class="sugguest-user-dt">
                        <a href="user_dashboard_activity.html"><img src="/assets/images/homepage/left-side/left-img-2.jpg" alt="" /></a>
                        <a href="user_dashboard_activity.html"><h4>Jassica William</h4></a>
                      </div>
                      <button class="request-btn"><i class="fas fa-user-plus"></i></button>
                    </div>
                    <div class="sugguest-user">
                      <div class="sugguest-user-dt">
                        <a href="user_dashboard_activity.html"><img src="/assets/images/homepage/left-side/left-img-3.jpg" alt="" /></a>
                        <a href="user_dashboard_activity.html"><h4>Rock</h4></a>
                      </div>
                      <button class="request-btn"><i class="fas fa-user-plus"></i></button>
                    </div>
                    <div class="sugguest-user">
                      <div class="sugguest-user-dt">
                        <a href="user_dashboard_activity.html"><img src="/assets/images/homepage/left-side/left-img-4.jpg" alt="" /></a>
                        <a href="user_dashboard_activity.html"><h4>Davil Smith</h4></a>
                      </div>
                      <button class="request-btn"><i class="fas fa-user-plus"></i></button>
                    </div>
                    <div class="sugguest-user">
                      <div class="sugguest-user-dt">
                        <a href="user_dashboard_activity.html"><img src="/assets/images/homepage/left-side/left-img-5.jpg" alt="" /></a>
                        <a href="user_dashboard_activity.html"><h4>Ricky Doe</h4></a>
                      </div>
                      <button class="request-btn"><i class="fas fa-user-plus"></i></button>
                    </div>
                  </div>								
                </div>
              </div>
              <div class="col-lg-6 col-md-7">						
                <div class="center-section">						
                  <div class="main-search-bar">						
                    <h2>Canal VODJ - Videos recientes</h2>
                    <form>
                      <div class="main-search-inputs">
                        <div class="row no-gutters">
                          <div class="col-lg-5 col-md-12 col-sm-12">
                            <input class="search-form-input" type="text" placeholder="Search events by categories" />													
                          </div>
                          <div class="col-lg-3 col-md-12 col-sm-12 border-lr">
                          </div>
                          <div class="col-lg-3 col-md-12 col-sm-12">
                            <input class="search-form-input datepicker-here" type="text" placeholder="Select Date" />
                          </div>
                          <div class="col-lg-1 col-md-12 col-sm-12">
                            <button class="search-btn" type="submit"><i class="fas fa-search"></i><span>Search</span></button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="main-tabs">
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab-upcoming">
                        <div class="main-posts">
                          {
                            this.videos.map((video) => {
                              return (
                              <div class="event-main-post">
                                <div class="event-top">
                                  <div class="event-top-left">
                                    <a onClick={(e) => this.goToEventDetail(e, video)}>
                                      <h4>{video.title}</h4>
                                    </a>
                                  </div>
                                  <div class="event-top-right">
                                    <div class="post-dt-dropdown dropdown">
                                      <span class="dropdown-toggle-no-caret"  role="button" data-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></span>
                                      <div class="dropdown-menu post-rt-dropdown dropdown-menu-right">
                                        <a class="post-link-item" href="#">Ocultar</a>
                                        <a class="post-link-item" href="#">Detalles</a>											
                                        <a class="post-link-item" href="#">Perfil usuario</a>
                                        <a class="post-link-item" href="#">Reportar</a>																									
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="event-main-image">
                                  <div class="main-photo">
                                    <div class="photo-overlay"></div>
                                    <img src={video.thumbnails.high.url} alt="" />
                                    <div class="post-buttons">
                                      <div class="left-buttons">
                                        <ul class="main-btns">
                                          <li><button class="main-btn-link" onClick={() => window.location.href = '#'}>Ver video</button></li>																	
                                        </ul>
                                      </div>
                                    </div>
                                  </div>														
                                </div>

                                <div class="event-go-dt">
                                  <p>{video.description}</p>
                                </div>

                                <div class="event-city-dt">
														      <ul class="city-dt-list">
                                    <li>
                                      <div class="it-items">
                                        <i class="fas fa-map-marker-alt"></i>
                                        <div class="list-text-dt">
                                          <span>Ciudad</span>
                                          <ins>Medellín</ins>
                                        </div>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="it-items">
                                        <i class="fas fa-calendar-alt"></i>
                                        <div class="list-text-dt">
                                          <span>Fecha</span>
                                          <ins>21 Nov 2019</ins>
                                        </div>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="it-items">
                                        <i class="fas fa-clock"></i>
                                        <div class="list-text-dt">
                                          <span>Hora</span>
                                          <ins>6 PM to 9 PM</ins>
                                        </div>
                                      </div>
                                    </li>
                                  </ul>
													      </div>
                                <div class="like-comments">
                                  <div class="left-comments">
                                    <a href="#" class="like-item" title="Like">
                                      <i class="fas fa-heart"></i>
                                      <span><ins>Me gusta</ins> 0</span>
                                    </a>
                                    <a href="#" class="like-item lc-left" title="Comment">
                                      <i class="fas fa-comment-alt"></i>
                                      <span><ins>Comentarios</ins> 0</span>
                                    </a>
                                  </div>
                                  <div class="right-comments">
                                    <a href="#" class="like-item" title="Share">
                                      <i class="fas fa-share-alt"></i>
                                      <span><ins>Compartir</ins> 0</span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              );
                            })
                          }
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="right-side-items">
                  <div class="post-event">									
                    <h6>Enterate de nuestro proximo streaming</h6>
                    <p>Todas las semanas tenemos diferentes invitados, mantente conectado</p>
                  </div>
                  <div class="explore-events">
                    <h4>Explorar Eventos</h4>
                    <ul class="explore-events-dt">
                      <li>
                        <a href="#">
                          <div class="explore-card exp-right light-blue">
                            <div class="explore-item-1">
                              <div class="explore-icon-bg icon-large-1">
                                <i class="fas fa-sun"></i>
                              </div>
                              <div class="explore-content">
                                <i class="fas fa-sun"></i>
                                <span>Hoy</span>
                              </div>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="explore-card exp-left light-yellow">
                            <div class="explore-item-1">
                              <div class="explore-icon-bg icon-large-1">
                                <i class="fas fa-cloud-sun"></i>
                              </div>
                              <div class="explore-content">
                                <i class="fas fa-cloud-sun"></i>
                                <span>Mañana</span>
                              </div>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="explore-card exp-right dark-blue">
                            <div class="explore-item-1">
                              <div class="explore-icon-bg icon-large-1">
                                <i class="fas fa-calendar-alt"></i>
                              </div>
                              <div class="explore-content">
                                <i class="fas fa-calendar-alt"></i>
                                <span>Esta semana</span>
                              </div>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="explore-card exp-left light-green">
                            <div class="explore-item-1">
                              <div class="explore-icon-bg icon-large-1">
                                <i class="fas fa-calendar-check"></i>
                              </div>
                              <div class="explore-content">
                                <i class="fas fa-calendar-check"></i>
                                <span>Buscar fecha</span>
                              </div>
                            </div>
                          </div>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="news-data full-width">
                    <div class="categories-left-heading">
                      <h3>Noticias</h3>							
                    </div>
                    <div class="categories-items">
                      <div class="news-item">
                        <div class="news-item-heading">
                          <i class="fas fa-music"></i>
                          <h6>Musica</h6>
                        </div>
                        <div class="news-description">
                          Gotshell lanzara un nuevo EP en un sello conocido.
                        </div>
                      </div>
                      <div class="news-item">
                        <div class="news-item-heading">
                          <i class="fas fa-pen-nib"></i>
                          <h6>Arte</h6>
                        </div>
                        <div class="news-description">
                          Colectivos, clubes, crece la escena electronica
                        </div>
                      </div>	
                      <div class="news-item">
                        <div class="news-item-heading">
                          <i class="far fa-futbol"></i>
                          <h6>Diseño</h6>
                        </div>
                        <div class="news-description">
                          Se acerca la feria de medellin music week
                        </div>
                      </div>																														
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="footer-left">
                <ul>
                  <li><a href="privacy_policy.html">Privacy</a></li>
                  <li><a href="term_conditions.html">Term and Conditions</a></li>
                  <li><a href="about.html">About</a></li>
                  <li><a href="contact_us.html">Contact Us</a></li>								
                </ul>
              </div>
            </div>					
            <div class="col-lg-6 col-md-12">
              <div class="footer-right">
                <ul class="copyright-text">
                  <li><a href="index.html"><img src="/assets/images/logo-2.svg" alt="" /></a></li>
                  <li><div class="ftr-1"><i class="far fa-copyright"></i> 2019 Goeveni by <a href="https://themeforest.net/user/gambolthemes">Gambolthemes</a>. All Rights Reserved.</div></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>);
  }
}
