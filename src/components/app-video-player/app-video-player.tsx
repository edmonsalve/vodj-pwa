import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-video-player',
  styleUrl: 'app-video-player.css',
  shadow: false,
})
export class AppVideoPlayer {

  render() {
    return (
      <div class="video-player-container">
        <div class="video-player-container-sections">
          <div class="video-player-container-sections-player">
            <iframe src="https://streaming.revmusic.app/vodj/" allowFullScreen frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
          </div>
          <div class="video-player-container-users-interaction">
            <div class="video-player-container-users-interaction-header">
              <img src="../assets/images/logo.png" alt=""/>
            </div>
            <div class="video-player-container-users-interaction-comments">
              <div class="video-player-container-users-interaction-comments-container">
                <div class="video-player-container-users-interaction-comments-comment">
                  <img src="../assets/images/dp.jpg" alt=""/>
                  <span class="">Mensaje de un usuario sasds ds Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur doloribus laborum accusantium molestias autem eum, corrupti minus quasi labore quas animi inventore? Blanditiis assumenda voluptatem velit sunt voluptas molestias placeat?</span>
                </div>
              </div>
              <div class="video-player-container-users-interaction-comments-write">
                <input type="text" name="" id=""/>
                <img src="../assets/images/video-player/send-message.png" alt=""/>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
