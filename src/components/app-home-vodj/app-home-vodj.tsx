import { Component, h } from '@stencil/core';
import { VodjUser } from '../../models/vodjUser';
import UserService from '../../services/user.service';

const nav = document.querySelector('ion-nav');

@Component({
  tag: 'app-home-vodj',
  styleUrl: 'app-home-vodj.css',
  shadow: false,
})
export class AppHomeVodj {


  loggedUser: VodjUser;
  userService: UserService = new UserService();

  componentWillLoad() {
    this.loggedUser = this.userService.getLoggedUser();
  }

  goToLogin() {
    nav.setRoot('app-login');
  }

  goToRegister() {
    nav.setRoot('app-register');
  }

  goToTimeline() {
    nav.rootParams = this.loggedUser;
    nav.setRoot('app-home');
  }

  goToProfile() {
    nav.setRoot('app-user-profile');
  }

  goToLivePlayer() {
    nav.setRoot("app-video-player")
  }

  render() {
    return (
      <div class="vodj-home-container">
        <header>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <nav class="navbar navbar-expand-lg navbar-light bg-dark1 justify-content-sm-start">
                  <a
                    class="order-1 order-lg-0 ml-lg-0 ml-3 mr-auto"
                    href="index.html"
                  >
                    <img src="../assets/images/logo.png" alt="" />
                  </a>
                  <button class="navbar-toggler align-self-start" type="button">
                    <i class="fas fa-bars"></i>
                  </button>
                  <div
                    class="collapse navbar-collapse d-flex flex-column flex-lg-row flex-xl-row justify-content-lg-end bg-dark1 p-3 p-lg-0 mt1-5 mt-lg-0 mobileMenu"
                    id="navbarSupportedContent"
                  >
                    <ul class="navbar-nav align-self-stretch">
                      <li class="nav-item active">
                        <a class="nav-link" href="index.html">
                          Home <span class="sr-only">(current)</span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" onClick={() => this.goToTimeline()}>
                          Timeline
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="discussions.html">
                          Personas
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="discussions.html">
                          Acerca de VODJ
                        </a>
                      </li>
                    </ul>
                    <a onClick={() => this.goToLivePlayer()} class="add-event">
                      Estamos en vivo!
                    </a>
                  </div>
                  <ul class="group-icons">
                    <li class="dropdown">
                      <a
                        href="#"
                        class="icon-set dropdown-toggle-no-caret"
                        role="button"
                        data-toggle="dropdown"
                      >
                        <i class="fas fa-envelope"></i>
                      </a>
                      <div class="dropdown-menu message-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Jassica William </div>
                                <span>Hey How are you John Doe...</span>
                              </a>
                            </div>
                            <div class="time5">2 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Rock Smith </div>
                                <span>
                                  Interesting Event! I will join this...
                                </span>
                              </a>
                            </div>
                            <div class="time5">5 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Joy Doe </div>
                                <span>Hey Sir! What about you...</span>
                              </a>
                            </div>
                            <div class="time5">10 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <a href="my_dashboard_messages.html" class="view-all">
                            View All Messages
                          </a>
                        </div>
                      </div>
                    </li>
                    <li class="dropdown">
                      <a
                        href="#"
                        class="icon-set dropdown-toggle-no-caret"
                        role="button"
                        data-toggle="dropdown"
                      >
                        <i class="fas fa-bell"></i>
                      </a>
                      <div class="dropdown-menu notification-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Jassica William </div>
                                <span>comment on your video.</span>
                              </a>
                            </div>
                            <div class="time5">2 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Rock Smith</div>
                                <span>your order is accepted.</span>
                              </a>
                            </div>
                            <div class="time5">5 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Joy Doe </div>
                                <span>your bill slip sent on your email.</span>
                              </a>
                            </div>
                            <div class="time5">10 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <a
                            href="my_dashboard_all_notifications.html"
                            class="view-all"
                          >
                            View All Notifications
                          </a>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <div class="account order-1 dropdown">
                    <a
                      href="#"
                      class="account-link dropdown-toggle-no-caret"
                      role="button"
                      data-toggle="dropdown"
                    >
                      <div class="user-dp">
                        <img src="../assets/images/dp.jpg" alt="" />
                      </div>
                      <span>{this.loggedUser.fullName}</span>
                      <i class="fas fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu account-dropdown dropdown-menu-right">
                      <a class="link-item" onClick={() => this.goToProfile()}>
                        Perfil
                      </a>
                      <a class="link-item" href="my_dashboard_messages.html">
                        Messages
                      </a>
                      <a
                        class="link-item"
                        href="my_dashboard_booked_events.html"
                      >
                        Booked Events
                      </a>
                      <a class="link-item" href="my_dashboard_credits.html">
                        Credit <span class="right-cm">$100</span>
                      </a>
                      <a class="link-item" href="invite.html">
                        Invite
                      </a>
                      <a
                        class="link-item"
                        href="my_dashboard_setting_info.html"
                      >
                        Setting
                      </a>
                      <a class="link-item" href="login.html">
                        Logout
                      </a>
                    </div>
                  </div>
                </nav>
                <div class="overlay"></div>
              </div>
            </div>
          </div>
        </header>
        <div class="vodj-video-container">
          <iframe src={`http://www.youtube.com/embed/s1cRzZbKNH0?origin=http://localhost:3333"&controls=1&autoplay=1`} frameborder='0'></iframe>"
        </div>
        <footer class="vodj-footer">
          <div class="container">
            <div class="home-row row">
              <div class="col-lg-3 col-md-12">
                <div class="footer-left">
                  Escucha nuestra radio en vivo
                </div>
              </div>
              <div class="col-lg-3 col-md-12 image-container">
                <img src="../assets/images/radio.png" alt=""/>
              </div>	
              <div class="col-lg-3 col-md-12">
                <div class="footer-right">
                  <ul class="copyright-text">
                    <li><div class="ftr-1"><i class="far fa-copyright"></i> 2020 VODJ All Rights Reserved.</div></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }

}
