import { Component, h, Prop } from '@stencil/core';
import { NotificationType } from '../../models/notifications';

@Component({
  tag: 'app-notifications',
  styleUrl: 'app-notifications.css',
  shadow: false,
})
export class AppNotifications {


  @Prop()
  notificationType: NotificationType;

  render() {

    if (!this.notificationType) {
      return;
    }

    if (this.notificationType.successfull) {
      return (
        <section>
          <div class="square_box box_three"></div>
          <div class="square_box box_four"></div>
          <div class="container mt-5">
            <div class="row">
              <div class="col-sm-12">
                <div class="alert fade alert-simple alert-success alert-dismissible text-left font__family-montserrat font__size-16 font__weight-light brk-library-rendered rendered show">
                  <button type="button" class="close font__size-18" data-dismiss="alert">
                    <span aria-hidden="true"><a href="https://www.youtube.com/watch?v=_XiOcsj3oNI&t=50s" target="_blank">
                      <i class="fa fa-times greencross"></i>
                      </a></span>
                    <span class="sr-only">Close</span> 
                  </button>
                  <i class="start-icon far fa-check-circle faa-tada animated"></i>
                  <strong class="font__weight-semibold">Well done!</strong> You successfullyread this important.
                </div>
              </div>
            </div>
          </div>
        </section>
      )
    } else  if (this.notificationType.error) {
      return (
        <section>
          <div class="square_box box_three"></div>
          <div class="square_box box_four"></div>
          <div class="container mt-5">
            <div class="row">
            <div class="col-sm-12">
              <div class="alert fade alert-simple alert-danger alert-dismissible text-left font__family-montserrat font__size-16 font__weight-light brk-library-rendered rendered show" role="alert" data-brk-library="component__alert">
                <button type="button" class="close font__size-18" data-dismiss="alert">
                  <span aria-hidden="true">
                    <i class="fa fa-times danger "></i>
                  </span>
                  <span class="sr-only">Cerrar</span>
                </button>
                <i class="start-icon far fa-times-circle faa-pulse animated"></i>
                <strong class="font__weight-semibold">Oh snap!</strong> Change a few things up and try submitting again.
              </div>
            </div>
            </div>
          </div>
        </section>
      )
    } else if (this.notificationType.warning) {
      return (
        <div class="col-sm-12">
          <div class="alert fade alert-simple alert-warning alert-dismissible text-left font__family-montserrat font__size-16 font__weight-light brk-library-rendered rendered show" role="alert" data-brk-library="component__alert">
            <button type="button" class="close font__size-18" data-dismiss="alert">
              <span aria-hidden="true">
                <i class="fa fa-times warning"></i>
              </span>
              <span class="sr-only">Close</span>
            </button>
            <i class="start-icon fa fa-exclamation-triangle faa-flash animated"></i>
            <strong class="font__weight-semibold">Warning!</strong> Better check yourself, you're not looking too good.
          </div>
        </div>
      );
    };
  }
}
