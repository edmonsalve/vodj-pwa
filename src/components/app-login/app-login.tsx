import { Component, h, State } from "@stencil/core";
import { User } from "../../models/firebaseUser";
import { validateEmail } from "../../utils/helpers";
import { NotificationType } from "../../models/notifications";
import { VodjUser } from "../../models/vodjUser";
import UserService from "../../services/user.service";

const nav = document.querySelector("ion-nav");

declare var firebase: any;

@Component({
  tag: "app-login",
  styleUrl: "app-login.css",
  shadow: false,
})
export class AppLogin {
  @State() showNotification: boolean = false;

  userService: UserService = new UserService();

  notificationType: NotificationType = {
    error: null,
    warning: null,
    successfull: null,
  };

  @State() isLoading: boolean = false;

  @State()
  user: User = {
    displayName: "",
    email: "",
    password: "",
  };

  vodjUser: VodjUser = {
    uid: "",
    followers: [],
    following: [],
    likes: [],
    fullName: "",
    ownVideos: [],
  };

  handleLogin(e) {
    this.isLoading = true;
    e.preventDefault();
    if (validateEmail(this.user.email) && this.user.password != "") {
      firebase
        .auth()
        .signInWithEmailAndPassword(this.user.email, this.user.password)
        .then((data) => {
          const userId = data.user.uid;
          this.userService.saveLoggedUser(this.vodjUser);
          this.getVodjUser(userId);
          this.isLoading = false;
          nav.rootParams = this.vodjUser;
          debugger;
          nav.setRoot("app-vodj-home");
        })
        .catch((error) => {
          this.processError(error);
          if (error) {
            this.notificationType.error = "Ocurrio un error creando el usuario";
          }
          this.isLoading = false
        });

    }
  }

  processError(error: any) {
    switch(error.code) {
      case "auth/user-not-found":
        this.notificationType.error = "No pudimos encontrar el usuario";
        break;
      default:
        break;
    }
  }

  getVodjUser(userId: string) {
    const dbRef = firebase.database().ref(`vodj-users/${userId}`);
    dbRef.once('value').then((snapshot) => {
      console.log(snapshot);
      debugger;
    })
  }

  handleChange(e) {
    let propertyName = e.target.name;
    this.user[propertyName] = e.target.value;
  }

  goToRegister() {
    nav.setRoot("app-register");
  }

  signUpOrUpWithGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('profile');
    provider.addScope('email');
    firebase.auth().signInWithPopup(provider).then(function(result) {
    // This gives you a Google Access Token.
    const token = result.credential.accessToken;
    // The signed-in user info.
    const user = result.user;

    if (token && user) {
      console.log(user);
    } else {
      console.log("something went wrong")
    }
});
  }

  render() {
    return (
      <body class="body-bg">
        {this.showNotification && (
          <app-notifications
            notificationType={this.notificationType}
          ></app-notifications>
        )}
        <main class="register-mp">
          <div class="main-section">
            <div class="container">
              <div class="row justify-content-md-center">
                <div class="col-md-10">
                  <div class="login-register-bg">
                    <div class="row no-gutters">
                      <div class="col-lg-6">
                        <div class="lg-left login-text-container">
                          <div class="lr-text">
                            <h2>Ingreso</h2>
                            <p>
                              Ingresa a VODJ, la plataforma especializada en musica electronica
                            </p>
                          </div>
                          <div class="lr-image">
                            <img src="../assets/images/logo.png" alt="" />
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="lr-right">
                          <div class="social-logins">
                            <button class="social-g-btn" onClick={() => this.signUpOrUpWithGoogle()}>
                              <i class="fab fa-google"></i>Continua con Google
                            </button>
                          </div>
                          <div class="or">Or</div>
                          <div class="login-register-form">
                            <form onSubmit={(e) => this.handleLogin(e)}>
                              <div class="form-group">
                                <input
                                  class="title-discussion-input"
                                  type="email"
                                  name="email"
                                  value={this.user.email}
                                  placeholder="Ingresa tu email"
                                  onInput={(event) => {
                                    this.handleChange(event);
                                  }}
                                />
                              </div>
                              <div class="form-group">
                                <input
                                  class="title-discussion-input"
                                  type="password"
                                  name="password"
                                  value={this.user.password}
                                  placeholder="Password"
                                  onInput={(event) => {
                                    this.handleChange(event);
                                  }}
                                />
                              </div>
                              <button class="login-btn" type="submit">
                                Ingresa ya!
                              </button>
                            </form>
                            <a href="#" class="forgot-link">
                              Olvidaste tu password?
                            </a>
                            <div class="regstr-link">
                              No tienes una cuenta?{" "}
                              <a
                                onClick={() => {
                                  this.goToRegister();
                                }}
                              >
                                Registrate ahora!
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <footer class="footer-bg">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 col-md-12">
                <div class="footer-left">
                  <ul>
                    <li>
                      <a href="privacy_policy.html">Privacidad</a>
                    </li>
                    <li>
                      <a href="term_conditions.html">Terminos y Condiciones</a>
                    </li>
                    <li>
                      <a href="about.html">Acerca de </a>
                    </li>
                    <li>
                      <a href="contact_us.html">Contactanos</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-6 col-md-12">
                <div class="footer-right">
                  <ul class="copyright-text">
                    <li>
                      <div class="ftr-1">
                        <i class="far fa-copyright"></i>
                        <a href="https://themeforest.net/user/gambolthemes">
                          2020 VODJ
                        </a>
                        Todos los derechos reservados
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </footer>
        {this.isLoading && <app-loader></app-loader>}
      </body>
    );
  }
}
