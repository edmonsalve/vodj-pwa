import { Component, h } from '@stencil/core';
@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {

  render() {
    return (
      <ion-app>
        <ion-router useHash={false}>
          <ion-route url="/" component="app-login" />
          <ion-route url="/home/" component="app-vodj-home" />
          <ion-route url="/timeline/" component="app-home" />
          <ion-route url="/login/" component="app-login" />
          <ion-route url="/register/" component="app-register" />
          <ion-route url="/people/" component="app-search-people" />
          <ion-route url="/video-details" component="app-video-detail" />
          <ion-route url="/user/details" component="app-user-profile" />
          <ion-route url="/search-people" component="app-search-people"></ion-route>
          <ion-route url="/video-player" component="app-video-player"></ion-route>
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
