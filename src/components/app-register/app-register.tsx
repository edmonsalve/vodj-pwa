import { Component, h, State } from "@stencil/core";
import { User } from "../../models/firebaseUser";
import { validateEmail } from "../../utils/helpers";
import { NotificationType } from "../../models/notifications";
import UserService from "../../services/user.service";
import { VodjUser } from "../../models/vodjUser";

declare var firebase: any;
const nav = document.querySelector("ion-nav");

@Component({
  tag: "app-register",
  styleUrl: "app-register.css",
  shadow: false,
})
export class AppRegister {
  @State()
  isLoading: boolean = false;

  @State()
  notificationType: NotificationType = {
    error: null,
    warning: null,
    successfull: null,
  };

  @State()
  showNotification: boolean = false;

  userService: UserService = new UserService();

  firebaseUser: User = {
    displayName: "",
    email: "",
    password: "",
  };

  vodjUser: VodjUser = {
    uid: "",
    followers: [],
    following: [],
    likes: [],
    fullName: "Edward Monsalve",
    ownVideos: [],
  };

  handleChange(e) {
    let propertyName = e.target.name;
    this.firebaseUser[propertyName] = e.target.value;
  }

  async handleRegister(e) {
    e.preventDefault();
    this.isLoading = true;
    if (this.isValidUser()) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(
          this.firebaseUser.email,
          this.firebaseUser.password
        )
        .then((response) => {
          this.vodjUser.uid = response.user.uid;
          this.vodjUser.fullName = this.firebaseUser.displayName;
          this.saveVodjUser();
          response.user.updateProfile({
            displayName: this.firebaseUser.displayName,
          });
          nav.rootParams = this.vodjUser;
		      nav.setRoot("app-home-vodj");
        })
        .catch((error) => {
          if (error) {
            this.notificationType.error = error.toString();
            this.showNotification = true;
          }
        });
    } else {
      this.isLoading = false;
      this.notificationType.error = "Revise los datos ingresados";
      this.showNotification = true;
    }
  }

  saveVodjUser() {
    this.userService.saveLoggedUser(this.vodjUser);
  }

  isValidUser(): boolean {
    if (
      !validateEmail(this.firebaseUser.email) &&
      this.firebaseUser.password != ""
    )
      return false;

    if (
      this.firebaseUser.displayName.length > 3 &&
      this.firebaseUser.password.length > 3
    ) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    return (
      <body class="body-bg">
        {this.showNotification && (
          <app-notifications
            notificationType={this.notificationType}
          ></app-notifications>
        )}
        <main class="register-mp">
          <div class="main-section">
            <div class="container">
              <div class="row justify-content-md-center">
                <div class="col-md-10">
                  <div class="login-register-bg">
                    <div class="row no-gutters">
                      <div class="col-lg-6">
                        <div class="lg-left">
                          <div class="lr-text">
                            <h2>Registrate Ahora</h2>
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipiscing
                              elit. Nulla interdum blandit felis a hendrerit.
                            </p>
                          </div>
                          <div class="lr-image-register">
                            <img src="../assets/images/logo.png" alt="" />
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="lr-right">
                          <h4>Registrate en VODJ</h4>
                          <div class="login-register-form">
                            <form onSubmit={(e) => this.handleRegister(e)}>
                              <div class="form-group">
                                <input
                                  class="title-discussion-input"
                                  type="text"
                                  name="displayName"
                                  placeholder="Full Name"
                                  onInput={(event) => {
                                    this.handleChange(event);
                                  }}
                                />
                              </div>
                              <div class="form-group">
                                <input
                                  class="title-discussion-input"
                                  type="email"
                                  name="email"
                                  placeholder="Email Address "
                                  onInput={(event) => {
                                    this.handleChange(event);
                                  }}
                                />
                              </div>
                              <div class="form-group">
                                <input
                                  class="title-discussion-input"
                                  type="password"
                                  name="password"
                                  placeholder="Password"
                                  onInput={(event) => {
                                    this.handleChange(event);
                                  }}
                                />
                              </div>
                              <div class="rgstr-dt-txt">
                                By clicking Sign Up, you agree to our{" "}
                                <a href="#">Terms</a>,{" "}
                                <a href="#">Data Policy</a> and{" "}
                                <a href="#">Cookie Policy</a>. You may receive
                                Email notifications from us and can opt out at
                                any time.
                              </div>
                              <button class="login-btn" type="submit">
                                Register Now
                              </button>
                              <div class="login-link">
                                If you have an account?{" "}
                                <a href="login.html">Login Now</a>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <footer class="footer-bg">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 col-md-12">
                <div class="footer-left">
                  <ul>
                    <li>
                      <a href="privacy_policy.html">Privacy</a>
                    </li>
                    <li>
                      <a href="term_conditions.html">Term and Conditions</a>
                    </li>
                    <li>
                      <a href="about.html">About</a>
                    </li>
                    <li>
                      <a href="contact_us.html">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-6 col-md-12">
                <div class="footer-right">
                  <ul class="copyright-text">
                    <li>
                      <div class="ftr-1">
                        <i class="far fa-copyright"></i> 2019 Goeveni by{" "}
                        <a href="https://themeforest.net/user/gambolthemes">
                          Gambolthemes
                        </a>
                        . All Rights Reserved.
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </footer>
        {this.isLoading && <app-loader></app-loader>}
      </body>
    );
  }
}
