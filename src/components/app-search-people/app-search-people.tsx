import { Component, h } from "@stencil/core";
import { VodjUser } from "../../models/vodjUser";
import UserService from "../../services/user.service";

@Component({
  tag: "app-search-people",
  styleUrl: "app-search-people.css",
  shadow: false,
})
export class AppSearchPeople {

  loggedUser: VodjUser;
  userService: UserService = new UserService();

  componentWillLoad() {
    this.loggedUser = this.userService.getLoggedUser();
  }

  render() {
    return (
      <div>
        <header>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <nav class="navbar navbar-expand-lg navbar-light bg-dark1 justify-content-sm-start">
                  <a
                    class="order-1 order-lg-0 ml-lg-0 ml-3 mr-auto"
                    href="index.html"
                  >
                    <img src="../assets/images/logo.png" alt="" />
                  </a>
                  <button class="navbar-toggler align-self-start" type="button">
                    <i class="fas fa-bars"></i>
                  </button>
                  <div
                    class="collapse navbar-collapse d-flex flex-column flex-lg-row flex-xl-row justify-content-lg-end bg-dark1 p-3 p-lg-0 mt1-5 mt-lg-0 mobileMenu"
                    id="navbarSupportedContent"
                  >
                    <ul class="navbar-nav align-self-stretch">
                      <li class="nav-item active">
                        <a class="nav-link" href="index.html">
                          Home <span class="sr-only">(current)</span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link">
                          Timeline
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="discussions.html">
                          Personas
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="discussions.html">
                          Acerca de VODJ
                        </a>
                      </li>
                    </ul>
                    <a href="add_new_event.html" class="add-event">
                      Estamos en vivo!
                    </a>
                  </div>
                  <ul class="group-icons">
                    <li>
                      <a href="search_result.html" class="icon-set">
                        <i class="fas fa-search"></i>
                      </a>
                    </li>
                    <li class="dropdown">
                      <a
                        href="#"
                        class="icon-set dropdown-toggle-no-caret"
                        role="button"
                        data-toggle="dropdown"
                      >
                        <i class="fas fa-user-plus"></i>
                      </a>
                      <div class="dropdown-menu user-request-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                              </a>
                              <a href="#" class="user-title">
                                Jassica William
                              </a>
                            </div>
                            <button class="accept-btn">Accept</button>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                              </a>
                              <a href="#" class="user-title">
                                Rock Smith
                              </a>
                            </div>
                            <button class="accept-btn">Accept</button>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                              </a>
                              <a href="#" class="user-title">
                                Joy Doe
                              </a>
                            </div>
                            <button class="accept-btn">Accept</button>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <a
                            href="my_dashboard_all_requests.html"
                            class="view-all"
                          >
                            View All Friend Requests
                          </a>
                        </div>
                      </div>
                    </li>
                    <li class="dropdown">
                      <a
                        href="#"
                        class="icon-set dropdown-toggle-no-caret"
                        role="button"
                        data-toggle="dropdown"
                      >
                        <i class="fas fa-envelope"></i>
                      </a>
                      <div class="dropdown-menu message-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Jassica William </div>
                                <span>Hey How are you John Doe...</span>
                              </a>
                            </div>
                            <div class="time5">2 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Rock Smith </div>
                                <span>
                                  Interesting Event! I will join this...
                                </span>
                              </a>
                            </div>
                            <div class="time5">5 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Joy Doe </div>
                                <span>Hey Sir! What about you...</span>
                              </a>
                            </div>
                            <div class="time5">10 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <a href="my_dashboard_messages.html" class="view-all">
                            View All Messages
                          </a>
                        </div>
                      </div>
                    </li>
                    <li class="dropdown">
                      <a
                        href="#"
                        class="icon-set dropdown-toggle-no-caret"
                        role="button"
                        data-toggle="dropdown"
                      >
                        <i class="fas fa-bell"></i>
                      </a>
                      <div class="dropdown-menu notification-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Jassica William </div>
                                <span>comment on your video.</span>
                              </a>
                            </div>
                            <div class="time5">2 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Rock Smith</div>
                                <span>your order is accepted.</span>
                              </a>
                            </div>
                            <div class="time5">5 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Joy Doe </div>
                                <span>your bill slip sent on your email.</span>
                              </a>
                            </div>
                            <div class="time5">10 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <a
                            href="my_dashboard_all_notifications.html"
                            class="view-all"
                          >
                            View All Notifications
                          </a>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <div class="account order-1 dropdown">
                    <a
                      href="#"
                      class="account-link dropdown-toggle-no-caret"
                      role="button"
                      data-toggle="dropdown"
                    >
                      <div class="user-dp">
                        <img src="../assets/images/dp.jpg" alt="" />
                      </div>
                      <span>{this.loggedUser.fullName}</span>
                      <i class="fas fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu account-dropdown dropdown-menu-right">
                      <a class="link-item" href="my_dashboard_activity.html">
                        Profile
                      </a>
                      <a class="link-item" href="my_dashboard_messages.html">
                        Messages
                      </a>
                      <a
                        class="link-item"
                        href="my_dashboard_booked_events.html"
                      >
                        Booked Events
                      </a>
                      <a class="link-item" href="my_dashboard_credits.html">
                        Credit <span class="right-cm">$100</span>
                      </a>
                      <a class="link-item" href="invite.html">
                        Invite
                      </a>
                      <a
                        class="link-item"
                        href="my_dashboard_setting_info.html"
                      >
                        Setting
                      </a>
                      <a class="link-item" href="login.html">
                        Logout
                      </a>
                    </div>
                  </div>
                </nav>
                <div class="overlay"></div>
              </div>
            </div>
          </div>
        </header>
        <main class="Search-results">
          <div class="main-section">
            <div class="container">
              <div class="row justify-content-md-center">
                <div class="col-lg-4 col-md-12">
                  <div class="search-bar-main">
                    <input
                      type="text"
                      class="search-1"
                      placeholder="Search peoples..."
                    />
                    <i class="fas fa-search srch-ic"></i>
                  </div>
                </div>
              </div>
            </div>
            <div class="all-search-events">
              <div class="container">
                <div class="row">
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-1">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-1.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>John Doe</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>India
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-2">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-2.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Rock Smith</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>Canada
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-3">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-3.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Joy William</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>Australia
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-4">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-4.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Johnson Smith</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>Newyork
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-5">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-5.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Joginder Singh</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>India
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-6">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-6.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Davinder Singh</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>Los Angels
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-7">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-7.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Amritpal</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>Australia
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-8">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-8.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Zoena Singh</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>Nepal
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-9">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-9.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Simon Doe</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>India
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-10">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-10.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Gaggu Smith</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>Turky
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-11">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-11.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Varun William</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>Bangladesh
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="user-data full-width">
                      <div class="user-profile">
                        <div class="userbg-dt dpbg-12">
                          <div class="usr-pic">
                            <img
                              src="../assets/images/find-peoples/user-12.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="user-main-details">
                          <h4>Johnson William</h4>
                          <span>
                            <i class="fas fa-map-marker-alt"></i>New Zealand
                          </span>
                        </div>
                        <ul class="follow-msg-dt">
                          <li>
                            <div class="msg-dt-sm">
                              <button class="msg-btn1">Message</button>
                            </div>
                          </li>
                          <li>
                            <div class="follow-dt-sm">
                              <button class="follow-btn1">Follow</button>
                            </div>
                          </li>
                        </ul>
                        <div class="profile-link">
                          <a href="user_dashboard_activity.html">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="main-loader search-loader">
                      <div class="spinner">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <footer>
          <div class="container">
            <div class="row">
              <div class="col-lg-6 col-md-12">
                <div class="footer-left">
                  <ul>
                    <li>
                      <a href="privacy_policy.html">Privacy</a>
                    </li>
                    <li>
                      <a href="term_conditions.html">Term and Conditions</a>
                    </li>
                    <li>
                      <a href="about.html">About</a>
                    </li>
                    <li>
                      <a href="contact_us.html">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-6 col-md-12">
                <div class="footer-right">
                  <ul class="copyright-text">
                    <li>
                      <a href="index.html">
                        <img src="../assets/images/logo-2.svg" alt="" />
                      </a>
                    </li>
                    <li>
                      <div class="ftr-1">
                        <i class="far fa-copyright"></i> 2019 Goeveni by{" "}
                        <a href="https://themeforest.net/user/gambolthemes">
                          Gambolthemes
                        </a>
                        . All Rights Reserved.
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}
