import { Component, h } from "@stencil/core";
import { VodjUser } from "../../models/vodjUser";
import UserService from "../../services/user.service";

declare var firebase: any;

@Component({
  tag: "app-user-profile",
  styleUrl: "app-user-profile.css",
  shadow: false,
})
export class AppUserProfile {


  loggedUser: VodjUser;
  userService: UserService = new UserService();

  componentWillLoad() {
    this.loggedUser = this.userService.getLoggedUser();
    console.log(this.loggedUser)
  }

  uploadImageToFirebase(imageType: string) {
    const storageRef = firebase.storage().ref(imageType);
    
  }



  render() {
    return (
      <div>
        <header>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <nav class="navbar navbar-expand-lg navbar-light bg-dark1 justify-content-sm-start">
                  <a
                    class="order-1 order-lg-0 ml-lg-0 ml-3 mr-auto"
                    href="index.html"
                  >
                    <img src="../assets/images/logo.png" alt="" />
                  </a>
                  <button class="navbar-toggler align-self-start" type="button">
                    <i class="fas fa-bars"></i>
                  </button>
                  <div
                    class="collapse navbar-collapse d-flex flex-column flex-lg-row flex-xl-row justify-content-lg-end bg-dark1 p-3 p-lg-0 mt1-5 mt-lg-0 mobileMenu"
                    id="navbarSupportedContent"
                  >
                    <ul class="navbar-nav align-self-stretch">
                      <li class="nav-item active">
                        <a class="nav-link" href="index.html">
                          Home <span class="sr-only">(current)</span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="discussions.html">
                          Timeline
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="our_blog.html">
                          Blog
                        </a>
                      </li>
                    </ul>
                    <a href="add_new_event.html" class="add-event">
                      Estamos en vivo!
                    </a>
                  </div>
                  <ul class="group-icons">
                    <li>
                      <a href="search_result.html" class="icon-set">
                        <i class="fas fa-search"></i>
                      </a>
                    </li>
                    <li class="dropdown">
                      <a
                        href="#"
                        class="icon-set dropdown-toggle-no-caret"
                        role="button"
                        data-toggle="dropdown"
                      >
                        <i class="fas fa-user-plus"></i>
                      </a>
                      <div class="dropdown-menu user-request-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <input onClick={() => {this.uploadImageToFirebase('users-background')}} type="file" name="avatar" accept="image/png, image/jpeg" hidden/>
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                              </a>
                              <a href="#" class="user-title">
                                Jassica William
                              </a>
                            </div>
                            <button class="accept-btn">Accept</button>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                              </a>
                              <a href="#" class="user-title">
                                Rock Smith
                              </a>
                            </div>
                            <button class="accept-btn">Accept</button>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                              </a>
                              <a href="#" class="user-title">
                                Joy Doe
                              </a>
                            </div>
                            <button class="accept-btn">Accept</button>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <a
                            href="my_dashboard_all_requests.html"
                            class="view-all"
                          >
                            View All Friend Requests
                          </a>
                        </div>
                      </div>
                    </li>
                    <li class="dropdown">
                      <a
                        href="#"
                        class="icon-set dropdown-toggle-no-caret"
                        role="button"
                        data-toggle="dropdown"
                      >
                        <i class="fas fa-envelope"></i>
                      </a>
                      <div class="dropdown-menu message-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">{this.loggedUser.fullName}</div>
                                <span>Hey How are you John Doe...</span>
                              </a>
                            </div>
                            <div class="time5">2 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Rock Smith </div>
                                <span>
                                  Interesting Event! I will join this...
                                </span>
                              </a>
                            </div>
                            <div class="time5">5 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Joy Doe </div>
                                <span>Hey Sir! What about you...</span>
                              </a>
                            </div>
                            <div class="time5">10 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <a href="my_dashboard_messages.html" class="view-all">
                            View All Messages
                          </a>
                        </div>
                      </div>
                    </li>
                    <li class="dropdown">
                      <a
                        href="#"
                        class="icon-set dropdown-toggle-no-caret"
                        role="button"
                        data-toggle="dropdown"
                      >
                        <i class="fas fa-bell"></i>
                      </a>
                      <div class="dropdown-menu notification-dropdown dropdown-menu-right">
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Jassica William </div>
                                <span>comment on your video.</span>
                              </a>
                            </div>
                            <div class="time5">2 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Rock Smith</div>
                                <span>your order is accepted.</span>
                              </a>
                            </div>
                            <div class="time5">5 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <div class="request-users">
                            <div class="user-request-dt">
                              <a href="#">
                                <img
                                  src="../assets/images/user-dp-1.jpg"
                                  alt=""
                                />
                                <div class="user-title1">Joy Doe </div>
                                <span>your bill slip sent on your email.</span>
                              </a>
                            </div>
                            <div class="time5">10 min ago</div>
                          </div>
                        </div>
                        <div class="user-request-list">
                          <a
                            href="my_dashboard_all_notifications.html"
                            class="view-all"
                          >
                            View All Notifications
                          </a>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <div class="account order-1 dropdown">
                    <a
                      href="#"
                      class="account-link dropdown-toggle-no-caret"
                      role="button"
                      data-toggle="dropdown"
                    >
                      <div class="user-dp">
                        <img src="../assets/images/dp.jpg" alt="" />
                      </div>
                      <span>Hi! John</span>
                      <i class="fas fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu account-dropdown dropdown-menu-right">
                      <a class="link-item" href="my_dashboard_activity.html">
                        Profile
                      </a>
                      <a class="link-item" href="my_dashboard_messages.html">
                        Messages
                      </a>
                      <a
                        class="link-item"
                        href="my_dashboard_booked_events.html"
                      >
                        Booked Events
                      </a>
                      <a class="link-item" href="my_dashboard_credits.html">
                        Credit <span class="right-cm">$100</span>
                      </a>
                      <a class="link-item" href="invite.html">
                        Invite
                      </a>
                      <a
                        class="link-item"
                        href="my_dashboard_setting_info.html"
                      >
                        Setting
                      </a>
                      <a class="link-item" href="login.html">
                        Logout
                      </a>
                    </div>
                  </div>
                </nav>
                <div class="overlay"></div>
              </div>
            </div>
          </div>
        </header>
        <main class="dashboard-mp">
          <div class="dash-todo-thumbnail-area1">
            <div
              class="todo-thumb1 dash-bg-image1 dash-bg-overlay"
            ></div>
            <div class="dash-todo-header1">
              <div class="container">
                <div class="row">
                  <div class="col-lg-12 col-md-12">
                    <div class="my-profile-dash">
                      <div class="my-dp-dash">
                        <img
                          src="../assets/images/my-dashboard/my-dp.jpg"
                          alt=""
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="dash-dts">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                  <div class="event-title">
                    <div class="my-dash-dt">
                      <h3>Rock William</h3>
                      <span>Miembro desde 2010</span>
                      <span>
                        <i class="fas fa-map-marker-alt"></i>Colombia
                      </span>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                  <ul class="right-details">
                    <li>
                      <div class="user-buttons">
                        <div class="user-follow">
                          <a href="#">Seguir</a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="all-dis-evnt">
                        <div class="dscun-txt">Seguidores</div>
                        <div class="dscun-numbr">22</div>
                      </div>
                    </li>
                    <li>
                      <div class="all-dis-evnt">
                        <div class="dscun-txt">Seguidos</div>
                        <div class="dscun-numbr">40</div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="dash-tab-links">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  <ul class="nav nav-tabs">
                    <li class="nav-item">
                      <a
                        class="nav-link active"
                        href="user_dashboard_about.html"
                      >
                        Acerca de
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link">
                        Seguidores <span class="badge badge-alrts">20</span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link">
                        Seguidos <span class="badge badge-alrts">20</span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link">
                        Le gusta <span class="badge badge-alrts">20</span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-3 col-md-5">
                  <div class="user-data full-width">
                    <div class="about-left-heading">
                      <h3>Informacion personal</h3>
                    </div>
                    <ul class="about-dt-items">
                      <li>
                        <div class="about-itdts">
                          <div class="about-1">Vive en</div>
                          <div class="about-2">India</div>
                        </div>
                      </li>
                      <li>
                        <div class="about-itdts">
                          <div class="about-1">Fecha de nacimiento</div>
                          <div class="about-2">29 Agosto 1990</div>
                        </div>
                      </li>
                      <li>
                        <div class="about-itdts">
                          <div class="about-1">Email</div>
                          <div class="about-2">rockwilliam@gmail.com</div>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="user-data full-width">
                    <div class="about-left-heading">
                      <h3>Redes Sociales</h3>
                    </div>
                    <div class="categories-items">
                      <a class="category-social-item" href="#">
                        <i
                          class="fab fa-facebook-square"
                          style={{ color: "#3b5998;" }}
                        ></i>
                        http://www.facebook.com
                      </a>
                      <a class="category-social-item" href="#">
                        <i
                          class="fab fa-twitter"
                          style={{ color: "#1da1f2;" }}
                        ></i>
                        http://www.twitter.com
                      </a>
                      <a class="category-social-item" href="#">
                        <i
                          class="fab fa-google-plus"
                          style={{ color: "#dd4b39;" }}
                        ></i>
                        http://www.googleplus.com
                      </a>
                      <a class="category-social-item" href="#">
                        <i
                          class="fab fa-instagram"
                          style={{ color: "#405de6;" }}
                        ></i>
                        http://www.instagram.com
                      </a>
                      <a class="category-social-item" href="#">
                        <i
                          class="fab fa-linkedin"
                          style={{ color: "#0077b5;" }}
                        ></i>
                        http://www.linkedin.com
                      </a>
                      <a class="category-social-item" href="#">
                        <i
                          class="fab fa-youtube"
                          style={{ color: "#ff0000;" }}
                        ></i>
                        http://www.youtube.com/
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-9 col-md-7">
                  <div class="user-data full-width">
                    <div class="about-left-heading">
                      <h3>{this.loggedUser.fullName}</h3>
                    </div>
                    <div class="about-dt-des">
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Praesent laoreet, dolor ut mollis rutrum, mauris arcu
                        mollis lacus, eget imperdiet neque neque eget nisl. Nunc
                        suscipit nulla dapibus nisi vestibulum tincidunt. Cras
                        vestibulum vel ante et porttitor. Duis luctus consequat
                        purus. Duis bibendum eget enim nec posuere. Aliquam
                        purus lectus, blandit aliquam enim nec, viverra egestas
                        libero. Suspendisse dictum neque et finibus posuere.{" "}
                      </p>
                    </div>
                  </div>
                  <div class="user-data full-width">
                    <div class="about-left-heading">
                      <h3>Hobbies</h3>
                    </div>
                    <div class="about-hobbies">
                      <div class="row">
                        <div class="col-lg-6 col-md-12">
                          <div class="all-hobbies">
                            <h6>Acerca de su musica</h6>
                            <span>Folk, Rap, Techno, House</span>
                          </div>
                          <div class="all-hobbies">
                            <h6>Dj productor</h6>
                            <span>
                              Novel, Comics, Jokes, Love Stories, Secience,
                              History
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <footer>
          <div class="container">
            <div class="row">
              <div class="col-lg-6 col-md-12">
                <div class="footer-left">
                  <ul>
                    <li>
                      <a href="privacy_policy.html">Privacidad</a>
                    </li>
                    <li>
                      <a href="term_conditions.html">Terminos y Condiciones</a>
                    </li>
                    <li>
                      <a href="about.html">About</a>
                    </li>
                    <li>
                      <a href="contact_us.html">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-6 col-md-12">
                <div class="footer-right">
                  <ul class="copyright-text">
                    <li>
                      <a href="index.html">
                        <img src="../assets/images/logo-2.svg" alt="" />
                      </a>
                    </li>
                    <li>
                      <div class="ftr-1">
                        <i class="far fa-copyright"></i> 2020 VODJ TV By{" "}
                        <a href="https://themeforest.net/user/gambolthemes">
                          VODJ
                        </a>
                        . Todos los derechos reservados.
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}
