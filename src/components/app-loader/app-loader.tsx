import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-loader',
  styleUrl: 'app-loader.css',
  shadow: false,
})
export class AppLoader {

  render() {
    return (
      <div class="loader-container">
        <div class="lds-ripple">
          <div></div>
          <div></div>
        </div>
      </div>
    );
  }

}
