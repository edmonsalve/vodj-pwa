import { Component, h, State } from '@stencil/core';

const nav = document.querySelector('ion-nav');

@Component({
  tag: 'app-video-detail',
  styleUrl: 'app-video-detail.css',
  shadow: false,
})
export class AppVideoDetail {

	@State() video: any;

	componentWillLoad() {
		this.video = nav.rootParams.video;
	}

  render() {

	if(!this.video) {
		return;
	}

    return (
      <div>
		<header>
			<div class="container">				
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<nav class="navbar navbar-expand-lg navbar-light bg-dark1 justify-content-sm-start">
							<a class="order-1 order-lg-0 ml-lg-0 ml-3 mr-auto" href="index.html"><img src="../assets/images/logo.png" alt=""/></a>
							<button class="navbar-toggler align-self-start" type="button">
								<i class="fas fa-bars"></i>
							</button>
							<div class="collapse navbar-collapse d-flex flex-column flex-lg-row flex-xl-row justify-content-lg-end bg-dark1 p-3 p-lg-0 mt1-5 mt-lg-0 mobileMenu" id="navbarSupportedContent">
								<ul class="navbar-nav align-self-stretch">
									<li class="nav-item active">
										<a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="discussions.html">Discussion</a>
									</li>									
									<li class="nav-item">
										<a class="nav-link" href="weather.html">Weather</a>
									</li>
									<li class="nav-item dropdown">
										<a href="#" class="nav-link dropdown-toggle-no-caret" role="button" data-toggle="dropdown">
											Pages
										</a>
										<div class="dropdown-menu pages-dropdown">
											<a class="link-item" href="login.html">Login</a>
											<a class="link-item" href="register.html">Register</a>											
											<a class="link-item" href="error_404.html">Error 404</a>
											<a class="link-item" href="categories.html">Categories</a>
											<a class="link-item" href="select_seats.html">Select Seats</a>
											<a class="link-item" href="find_friends.html">Find Friends</a>
											<a class="link-item" href="user_dashboard_activity.html">User Detail View</a>
											<a class="link-item" href="checkout.html">Checkout</a>
											<a class="link-item" href="confirmed_order.html">Confirmed Order</a>
											<a class="link-item" href="about.html">About</a>
											<a class="link-item" href="contact_us.html">Contact</a>
										</div>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="our_blog.html">Blog</a>
									</li>
								</ul>
								<a href="add_new_event.html" class="add-event">Add New Event</a>
							</div>
							<ul class="group-icons">
								<li><a href="search_result.html" class="icon-set"><i class="fas fa-search"></i></a></li>
								<li class="dropdown">
									<a href="#" class="icon-set dropdown-toggle-no-caret" role="button" data-toggle="dropdown">
										<i class="fas fa-user-plus"></i>
									</a>
									<div class="dropdown-menu user-request-dropdown dropdown-menu-right">
										<div class="user-request-list">
											<div class="request-users">
												<div class="user-request-dt">
													<a href="#"><img src="../assets/images/user-dp-1.jpg" alt=""/></a>
													<a href="#" class="user-title">Jassica William</a>
												</div>
												<button class="accept-btn">Accept</button>
											</div>											
										</div>	
										<div class="user-request-list">
											<div class="request-users">
												<div class="user-request-dt">
													<a href="#"><img src="../assets/images/user-dp-1.jpg" alt=""/></a>
													<a href="#" class="user-title">Rock Smith</a>
												</div>
												<button class="accept-btn">Accept</button>
											</div>											
										</div>	
										<div class="user-request-list">
											<div class="request-users">
												<div class="user-request-dt">
													<a href="#"><img src="../assets/images/user-dp-1.jpg" alt=""/></a>
													<a href="#" class="user-title">Joy Doe</a>
												</div>
												<button class="accept-btn">Accept</button>
											</div>											
										</div>
										<div class="user-request-list">
											<a href="my_dashboard_all_requests.html" class="view-all">View All Friend Requests</a>
										</div>	
									</div>
								</li>
								<li class="dropdown">
									<a href="#" class="icon-set dropdown-toggle-no-caret" role="button" data-toggle="dropdown">
										<i class="fas fa-envelope"></i>
									</a>
									<div class="dropdown-menu message-dropdown dropdown-menu-right">
										<div class="user-request-list">
											<div class="request-users">
												<div class="user-request-dt">
													<a href="#"><img src="../assets/images/user-dp-1.jpg" alt=""/>
														<div class="user-title1">Jassica William </div>
														<span>Hey How are you John Doe...</span>
													</a>
												</div>
												<div class="time5">2 min ago</div>
											</div>											
										</div>
										<div class="user-request-list">
											<div class="request-users">
												<div class="user-request-dt">
													<a href="#"><img src="../assets/images/user-dp-1.jpg" alt=""/>
														<div class="user-title1">Rock Smith </div>
														<span>Interesting Event! I will join this...</span>
													</a>
												</div>
												<div class="time5">5 min ago</div>
											</div>											
										</div>	
										<div class="user-request-list">
											<div class="request-users">
												<div class="user-request-dt">
													<a href="#"><img src="../assets/images/user-dp-1.jpg" alt=""/>
														<div class="user-title1">Joy Doe </div>
														<span>Hey Sir! What about you...</span>
													</a>
												</div>
												<div class="time5">10 min ago</div>
											</div>											
										</div>	
										<div class="user-request-list">
											<a href="my_dashboard_messages.html" class="view-all">View All Messages</a>
										</div>	
									</div>
								</li>
								<li class="dropdown">
									<a href="#" class="icon-set dropdown-toggle-no-caret" role="button" data-toggle="dropdown">
										<i class="fas fa-bell"></i>
									</a>
									<div class="dropdown-menu notification-dropdown dropdown-menu-right">
										<div class="user-request-list">
											<div class="request-users">
												<div class="user-request-dt">
													<a href="#"><img src="../assets/images/user-dp-1.jpg" alt=""/>
														<div class="user-title1">Jassica William </div>
														<span>comment on your video.</span>
													</a>
												</div>
												<div class="time5">2 min ago</div>
											</div>											
										</div>
										<div class="user-request-list">
											<div class="request-users">
												<div class="user-request-dt">
													<a href="#"><img src="../assets/images/user-dp-1.jpg" alt=""/>
														<div class="user-title1">Rock Smith</div>
														<span>your order is accepted.</span>
													</a>
												</div>
												<div class="time5">5 min ago</div>
											</div>											
										</div>	
										<div class="user-request-list">
											<div class="request-users">
												<div class="user-request-dt">
													<a href="#"><img src="../assets/images/user-dp-1.jpg" alt="" />
														<div class="user-title1">Joy Doe </div>
														<span>your bill slip sent on your email.</span>
													</a>
												</div>
												<div class="time5">10 min ago</div>
											</div>											
										</div>	
										<div class="user-request-list">
											<a href="my_dashboard_all_notifications.html" class="view-all">View All Notifications</a>
										</div>	
									</div>
								</li>
							</ul>
							<div class="account order-1 dropdown">
								<a href="#" class="account-link dropdown-toggle-no-caret" role="button" data-toggle="dropdown"> 
									<div class="user-dp"><img src="../assets/images/dp.jpg" alt=""/></div>
									<span>Hi! John</span>
									<i class="fas fa-angle-down"></i>
								</a>
								<div class="dropdown-menu account-dropdown dropdown-menu-right">
									<a class="link-item" href="my_dashboard_activity.html">Profile</a>
									<a class="link-item" href="my_dashboard_messages.html">Messages</a>											
									<a class="link-item" href="my_dashboard_booked_events.html">Booked Events</a>
									<a class="link-item" href="my_dashboard_credits.html">Credit <span class="right-cm">$100</span></a>
									<a class="link-item" href="invite.html">Invite</a>
									<a class="link-item" href="my_dashboard_setting_info.html">Setting</a>
									<a class="link-item" href="login.html">Logout</a>									
								</div>
							</div>							
						</nav>
						<div class="overlay"></div>
					</div>					
				</div>					
			</div>
		</header>
		<main class="event-mp">	
			<div class="event-todo-thumbnail-area">
				<div class="todo-thumb event-bg-image event-bg-overlay" style={{backgroundImage: `url(${this.video.thumbnails.high.url})`}}></div>
				<div class="event-todo-header">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 col-md-12">							
								<div class="my-profile-dt">
									<div class="my-dp-dt">
										<img src="../assets/images/event-view/my-dp.jpg" alt=""/>
									</div>
									<div class="my-text-dt">
										<h3>{this.video.channelTitle}</h3>
										<span>Organizador</span>
										<div class="profile-fw-btn">
											<a href="#" class="fw-btn">Seguir</a>
										</div>
									</div>
								</div>														
							</div>
							<div class="col-lg-6 col-md-12">		
								<ul class="comment-likes">
									<li>
										<a href="#" class="profile-likes">
											<i class="fas fa-heart"></i>
											Me gusta <ins>251</ins>
										</a>
									</li>
									<li>
										<a href="#" class="profile-likes">
											<i class="fas fa-comment-alt"></i>
											Comentarios <ins>10</ins>
										</a>
									</li>
									<li>
										<a href="#" class="profile-likes">
											<i class="fas fa-share-alt"></i>
											Compartir <ins>251</ins>
										</a>
									</li>
									<li class="dropdown">
										<a href="#" class="profile-likes dropdown-toggle-no-caret"  role="button" data-toggle="dropdown">
											<i class="fas fa-ellipsis-v"></i>
										</a>
										<div class="dropdown-menu post-rt-dropdown dropdown-menu-right">
											<a class="post-link-item" href="#">Esconder</a>
											<a class="post-link-item" href="#">Copiar Url</a>											
											<a class="post-link-item" href="#">Reportar</a>																									
										</div>
									</li>
								</ul>
							</div>											
						</div>
					</div>
				</div>
			</div>
			<div class="event-dts">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="event-title">
								<h2>{this.video.title}</h2>								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="event-sections">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="event-itm1 full-width">
								<div class="event-item-heading">
									<i class="fas fa-bars"></i>
									Descripcion
								</div>
								<div class="event-item-description">
									<p>{this.video.description}</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row video-container">
						<iframe src={`http://www.youtube.com/embed/${this.video.id}?origin=http://localhost:3333.com"&controls=0&autoplay=1`} frameborder='0'></iframe>"
					</div>
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="event-itm1 full-width">
								<div class="event-item-heading">
									<i class="fas fa-comment-alt"></i>
									Comentarios
								</div>
								<div class="event-item-description event-cmt-left">
									<div class="event-post-comment">
										<div class="event-post-bg">
											<div class="commntr-dp">
												<img src="images/event-view/user-1.jpg" alt=""/>
											</div>
											<form>
												<input class="ecomment-post" type="text" placeholder="Write a comment"/>
												<button class="event-post-btn" type="submit">Post Comment</button>
											</form>
										</div>
									</div>									
									<div class="event-comnt-bg">
										<div class="ecmnt-dt">
											<div class="event-usr-dt">
												<img src="../assets/images/event-view/user-2.jpg" alt=""/>
												<a href="user_dashboard_activity.html"><h4>Rock Smith</h4></a>												
											</div>
											<p>Sed pulvinar non tellus non venenatis. Mauris urna lacus, ornare quis purus vel, tempor placerat leo. Nunc massa nibh, viverra eu nisl vitae, ornare luctus nisi. Vivamus dictum sed felis vel sollicitudin.</p>
										</div>
										<div class="ereply-dt">
											<div class="event-usr-dt2">
												<img src="../assets/images/event-view/my-reply-dp.jpg" alt="" />
												<a href="#"><h4>John Doe</h4></a>
											</div>
											<p>Thanks</p>
										</div>
										<div class="event-post-reply">
											<div class="reply-dp1">
												<img src="../assets/images/event-view/user-1.jpg" alt="" />
											</div>
											<form>
												<input class="ereply-post" type="text" placeholder="Write a reply" />
												<button class="reply-post-btn" type="submit">Reply</button>
											</form>
										</div>
									</div>
									<div class="event-comnt-bg">
										<div class="ecmnt-dt">
											<div class="event-usr-dt">
												<img src="../assets/images/event-view/user-3.jpg" alt="" />
												<a href="user_dashboard_activity.html"><h4>Jassica William</h4></a>												
											</div>
											<p>Curabitur ac eros tempus, vestibulum odio non, rhoncus enim. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus pretium, lorem in convallis tempor, nisi nisl aliquam eros, dapibus imperdiet ante est eget mauris.</p>
										</div>										
										<div class="event-post-reply">
											<div class="reply-dp1">
												<img src="../assets/images/event-view/user-1.jpg" alt="" />
											</div>
											<form>
												<input class="ereply-post" type="text" placeholder="Write a reply" />
												<button class="reply-post-btn" type="submit">Reply</button>
											</form>
										</div>
									</div>									
									<div class="no-more">No More Comments</div>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</main>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-12">
						<div class="footer-left">
							<ul>
								<li><a href="privacy_policy.html">Privacy</a></li>
								<li><a href="term_conditions.html">Term and Conditions</a></li>
								<li><a href="about.html">About</a></li>
								<li><a href="contact_us.html">Contact Us</a></li>								
							</ul>
						</div>
					</div>					
					<div class="col-lg-6 col-md-12">
						<div class="footer-right">
							<ul class="copyright-text">
								<li><a href="index.html"><img src="../assets/images/logo-2.svg" alt=""/></a></li>
								<li><div class="ftr-1"><i class="far fa-copyright"></i> 2019 Goeveni by <a href="https://themeforest.net/user/gambolthemes">Gambolthemes</a>. All Rights Reserved.</div></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
	
	</div>
    );
  }

}
