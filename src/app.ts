import '@ionic/core';
// import { setupConfig } from '@ionic/core';

declare var firebase: any;

export default () => {
  // setupConfig({
  //   mode: 'ios'
  // });

  const firebaseConfig = {
    apiKey: "AIzaSyD5dfQWS5ZCHXUoit41yg_UcYH1D90vJtw",
    authDomain: "vodj-pwa.firebaseapp.com",
    databaseURL: "https://vodj-pwa.firebaseio.com",
    projectId: "vodj-pwa",
    storageBucket: "vodj-pwa.appspot.com",
    messagingSenderId: "86014334095",
    appId: "1:86014334095:web:fefdfedff9cc0e497e4cfa",
    measurementId: "G-2HWJY0TZLG"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
}