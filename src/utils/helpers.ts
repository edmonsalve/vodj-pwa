

export function sayHello() {
  return Math.random() < 0.5 ? 
  'Hello' : 'Hola';
}

export function validateEmail(email) {

  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return (true)
  }
  return (false)
}
