export default class DatabaseReference {

    private constructor() {

    }

    private static db: DatabaseReference = new DatabaseReference();
    
    public static getDatabaseReference() {
        return this.db;
    }
}